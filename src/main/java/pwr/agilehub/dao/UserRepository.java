package pwr.agilehub.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pwr.agilehub.model.User;

/**
 * Created by ewelina on 2015-12-22.
 */
public interface UserRepository extends CrudRepository<User, Long> {

    User findByUsername(String username);
}