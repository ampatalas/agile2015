package pwr.agilehub.dao;

import org.springframework.data.repository.CrudRepository;
import pwr.agilehub.model.Authority;
import pwr.agilehub.model.User;

/**
 * Created by ewelina on 2016-01-15.
 */
public interface AuthorityRepository extends CrudRepository<Authority, Long> {
    String findAuthorityByUsername(String username);
}
