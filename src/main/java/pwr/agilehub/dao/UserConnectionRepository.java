package pwr.agilehub.dao;

import org.springframework.data.repository.CrudRepository;
import pwr.agilehub.model.UserConnection;

/**
 * Created by ewelina on 2016-01-16.
 */
public interface UserConnectionRepository extends CrudRepository<UserConnection, String> {
   UserConnection findByDisplayName(String userId);
 }
