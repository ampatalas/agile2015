package pwr.agilehub.controler;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.security.SocialAuthenticationToken;
import org.springframework.social.security.SocialUserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import pwr.agilehub.businessLogic.IssueMovedData;
import pwr.agilehub.businessLogic.Task;
import pwr.agilehub.model.UserDetail;

import java.security.Principal;

/**
 * Created by ewelina on 2016-01-08.
 */
@Controller
public class MainController {

    @RequestMapping({"/", "/signin", "/main", "/kanban"})
    public String home( Principal principal)
    {
       return "home.html";

    }

    @RequestMapping({"/stats"})
    public String stats()
    {
        return "statistics.html";
    }

    @RequestMapping({"/about"})
    public String about()
    {
        return "aboutus.html";
    }

    @RequestMapping({"/GetKanabanDetails"})
    public @ResponseBody
    String GetKanabanDetails()
    {
        /*
            To display kanban for a project I need jason in below formt returned.
            Will kanban be generated for one givne project only ? If not the project parameter should
            be added to controllers method. Task's ID is required I think.
        */

        // Please, do not delete these comments!!! I may need them later.
        /*
        "name": "Kanban Board",
            "numberOfColumns": 3,
            "columns": [
        {"name": "TO DO", "cards": [
            {"title": "Explore new IDE for Development",
                    "details": ""},
            {"title": "Get new resource for new Project",
                    "details": ""}
            ]},
        {"name": "IN PROGRESS", "cards": [
            {"title": "Develop ui",
                    "details": "Testing Card Details"},
            {"title": "Develop backend",
                    "details": ""}
            ]},
        {"name": "DONE", "cards": [
            {"title": "Test user module",
                    "details": ""},
            {"title": "End to End Testing",
                    "details": ""},
            {"title": "CI module",
                    "details": ""}
            ]}
        ]

        };*/

        // First approach - send explicitly defined KanbanBoard data
        return "{\"name\": \"Kanban Board 1\", \"numberOfColumns\": 3, \"columns\": [{\"name\": \"TO DO 1\", \"cards\": [{\"title\": \"Explore new IDE for Development 1\",\"details\": \"\"},{\"title\": \"Get new resource for new Project 1\",\"details\": \"\"}]},{\"name\": \"IN PROGRESS 1\", \"cards\": [{\"title\": \"Develop ui\",\"details\": \"Testing Card Details 1\"},{\"title\": \"Develop backend\",\"details\": \"\"}]},{\"name\": \"DONE 1\", \"cards\": [{\"title\": \"Test user module 1\",\"details\": \"\"},{\"title\": \"End to End Testing 1\",\"details\": \"\"},{\"title\": \"CI module\",\"details\": \"\"}]}]}";
    }

    @RequestMapping({"/AddNewTask"})
    public @ResponseBody int AddNewTask(Task task)
    {
        //TODO - AddNew Task code here
        return 1;
    }

    @RequestMapping({"/IssueMoved"})
    public @ResponseBody int IssueMoved(IssueMovedData data)
    {
        //TODO - AddNew Task code here
        return 1;
    }
//    private GitHub github;
//
//    @Autowired
//    public MainController(GitHub github) {
//        this.github = github;
//    }
////    @RequestMapping(path = "/", method= RequestMethod.GET)
//    public String helloFacebook(Model model) {
//        if (!github.isAuthorized()) {
//            return "redirect:/auth/github";
//        }
//
//        model.addAttribute(github.userOperations().getUserProfile());
//        String homeFeed = github.userOperations().getUserProfile().getName();
//        model.addAttribute("feed", homeFeed);
//
//        return "home.html";
//    }
}