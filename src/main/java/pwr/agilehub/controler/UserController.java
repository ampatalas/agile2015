package pwr.agilehub.controler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.support.OAuth2Connection;
import org.springframework.social.security.SocialAuthenticationToken;
import org.springframework.social.security.SocialUserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pwr.agilehub.dao.UserConnectionRepository;
import pwr.agilehub.model.UserConnection;

import java.security.Principal;

/**
 * Created by ewelina on 2016-01-16.
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserConnectionRepository userConnectionRepository;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public UserConnection getUser(Principal principal)
    {
        if(principal != null){

          String username = principal.getName();
            UserConnection userConnection = userConnectionRepository.findByDisplayName(username);
            return userConnection;
        }
        return null;
    }
}
