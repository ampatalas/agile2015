package pwr.agilehub.controler;

import org.springframework.social.github.api.GistOperations;
import org.springframework.social.github.api.GitHub;
import org.springframework.social.github.api.RepoOperations;
import org.springframework.social.github.api.UserOperations;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestOperations;

/**
 * Created by ewelina on 2016-01-08.
 */
@Controller
public class LoginController {

    @RequestMapping("/log")
    public String login()
    {
        return "login.html";
    }
}