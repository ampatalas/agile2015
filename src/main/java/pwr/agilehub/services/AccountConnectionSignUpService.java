package pwr.agilehub.services;

/**
 * Created by ewelina on 2016-01-08.
 */
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionSignUp;
import org.springframework.social.connect.UserProfile;
import pwr.agilehub.dao.UsersDao;

public class AccountConnectionSignUpService implements ConnectionSignUp {

    private final UsersDao usersDao;

    public AccountConnectionSignUpService(UsersDao usersDao) {
        this.usersDao = usersDao;
    }

    public String execute(Connection<?> connection) {
        UserProfile profile = connection.fetchUserProfile();
        usersDao.createUser(profile.getUsername());
        return profile.getUsername();
    }

}