package pwr.agilehub.businessLogic;

/**
 * Created by Wojtek on 16.01.2016.
 *
 * Kalsa dotycząca danych przesyłanych wskutek przeniesienia taska z jednej kolumny do innej.
 * issueId - identyfikator taska
 * fromColumn - nazwa kolumny źrodłowej (TO DO, IN PROGRESS lub DONE)
 * fromPosition - pozycja taska w kolumnie źródłowej - 0, 1 ,2 ... liczone od góry kolumny
 * toColumn, toPosition - analogicznie jak dwa poprzednie, ale dla kolumny docelowej
 */
public class IssueMovedData {

    private int issueId;
    private String fromColumn;
    private int fromPosition;
    private String toColumn;
    private int toPosition;

    public int getIssueId() {
        return issueId;
    }

    public void setIssueId(int issueId) {
        this.issueId = issueId;
    }

    public int getToPosition() {
        return toPosition;
    }

    public void setToPosition(int toPosition) {
        this.toPosition = toPosition;
    }

    public String getFromColumn() {
        return fromColumn;
    }

    public void setFromColumn(String fromColumn) {
        this.fromColumn = fromColumn;
    }

    public int getFromPosition() {
        return fromPosition;
    }

    public void setFromPosition(int fromPosition) {
        this.fromPosition = fromPosition;
    }

    public String getToColumn() {
        return toColumn;
    }

    public void setToColumn(String toColumn) {
        this.toColumn = toColumn;
    }
}
