package pwr.agilehub.businessLogic;

/**
 * Created by Wojtek on 10.01.2016.
 *
 * This is Business Logic class that denotes github task.
 * If dao Task class is enough this BL level class can be removed and dao Task used instead.
 * Additional task attributes have to be introduced here.
 */
public class Task {
//    private int id;
//    public String getID() {return this.id;}
//    public void setID(int id) {this.id = id;}

    private String name;
    public String getName() {return this.name;}
    public void setName(String name) {this.name = name;}

}

