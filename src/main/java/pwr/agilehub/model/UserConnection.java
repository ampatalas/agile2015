package pwr.agilehub.model;

import lombok.Data;

import javax.persistence.*;

/**
 * Created by ewelina on 2016-01-16.
 */

//@Table(name="/User" + "/Connection'")
@Entity(name = "UserConnection")
@Data
public class UserConnection {

    @Id
    private String userId;

    @Column(name="providerId")
    private String providerId;

    @Column(name="providerUserId")
    private String providerUserId;

    @Column(name="rank")
    private int rank;

    @Column(name="displayName")
    private String displayName;

    @Column(name="profileUrl")
    private String profileUrl;

    @Column(name="imageUrl")
    private String imageUrl;

    @Column(name="accessToken")
    private String accessToken;

    @Column(name="secret")
    private String secret;

    @Column(name="refreshToken")
    private String refreshToken;

    @Column(name="expireTime")
    private Long expireTime;

}
