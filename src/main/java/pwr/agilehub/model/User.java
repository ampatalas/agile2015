package pwr.agilehub.model;

import lombok.Data;

import javax.persistence.*;

/**
 * Created by ewelina on 2015-12-22.
 */
@Entity
@Table(name="users")
@Data
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "username", length = 100)
    private String username;

    @Column(name = "password", length = 100)
    private String password;

    @Column(name = "enabled")
    private boolean enabled;

}