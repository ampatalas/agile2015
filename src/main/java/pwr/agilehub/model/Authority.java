package pwr.agilehub.model;

import lombok.Data;

import javax.persistence.*;

/**
 * Created by ewelina on 2016-01-15.
 */
@Data
@Entity
@Table(name="authorities")
public class Authority {

    @Id
    @GeneratedValue
    private long id;

    @Column
    private String username;

    @Column
    private String authority;
}
