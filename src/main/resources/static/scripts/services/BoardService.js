/*jshint undef: false, unused: false, indent: 2*/
/*global angular: false */

'use strict';

angular.module('demoApp').service('BoardService', ['$modal', 'BoardManipulator', 'BoardDataRepository', function ($modal, BoardManipulator, BoardDataRepository) {

    return {
        removeCard: function (board, column, card) {
            if (confirm('Are You sure to Delete?')) {
                BoardManipulator.removeCardFromColumn(board, column, card);
            }
        },

        addNewCard: function (board, column, refresher) {
            var modalInstance = $modal.open({
                templateUrl: 'views/newCard.html',
                controller: 'NewCardController',
                backdrop: 'static',
                resolve: {
                    column: function () {
                        return column;
                    }
                }
            });
            /*      modalInstance.result.then(function (cardDetails) {
             if (cardDetails) {
             BoardManipulator.addCardToColumn(board, cardDetails.column, cardDetails.title, cardDetails.details);
             this.kanbanBoard(BoardDataRepository.getData().then(function successCallback(response) {
             board =  response.data;
             }, function errorCallback(response) {
             return;
             }));
             }
             });*/
            modalInstance.result.then(function (cardDetails) {
                if (cardDetails) {
                    // BoardManipulator.addCardToColumn(board, cardDetails.column, cardDetails.title, cardDetails.details);
                    BoardDataRepository.addNewCard(cardDetails)
                        .then(function successCallback(response) {
                            refresher();
                        });
                }
            });
        },

        kanbanBoard: function (repo) {
            var boardName = "Kanban board for repository " + repo.name;
            var numberOfColumns = 3;
            var columnNames = ["TO DO", "IN PROGRESS", "DONE"];

            var kanbanBoard = new Board(boardName, numberOfColumns);
            angular.forEach(columnNames, function (columnName) {
                BoardManipulator.addColumn(kanbanBoard, columnName);
            });
            angular.forEach(repo.issues, function (issue) {
                BoardManipulator.addCardToBoard(kanbanBoard, issue);
            });
            return kanbanBoard;
        }
    };
}]);