/*jshint undef: false, unused: false, indent: 2*/
/*global angular: false */

'use strict';

angular.module('demoApp').service('GitHubDataRepository', ['$q', '$http', function ($q, $http) {

    return {
        getUserData: function () {
            var deferred = $q.defer();
            $http.get("http://localhost:8080/user")
                .success(function (user) {
                    deferred.resolve(user);
                });
            return deferred.promise;
        },

        getUserDetails: function (username) {
            var deferred = $q.defer();
            $http.get("https://api.github.com/users/" + username)
                .success(function (data) {
                    deferred.resolve(data);
                });
            return deferred.promise;
        },

        getUserGithubRepos: function(username) {
            var deferred = $q.defer();
            $http.get("https://api.github.com/users/" + username + "/repos")
                .success(function(repos) {
                    deferred.resolve(repos);
                });
            return deferred.promise;
        },

        getGithubRepoIssues: function(githubRepo) {
            var deferred = $q.defer();
            $http.get("https://api.github.com/repos/" +  githubRepo.owner.login + "/" + githubRepo.name + "/issues?state=all")
                .success(function(issues) {
                    deferred.resolve(issues);
                });
            return deferred.promise;
        },

        getUserAvatar: function (username) {
            var deferred = $q.defer();
            $http.get("https://api.github.com/users/" + username)
                .success(function (user) {
                    deferred.resolve(user.avatar_url);
                });
            return deferred.promise;
        }
    }
}]);