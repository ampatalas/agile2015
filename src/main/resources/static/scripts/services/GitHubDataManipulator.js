/*jshint undef: false, unused: false, indent: 2*/
/*global angular: false */
/**
 * Created by Wojtek on 16.01.2016.
 */
'use strict';

angular.module('demoApp').service('GitHubDataManipulator', function () {

    return {
        getRepoById: function (repos, repoId) {
            var repo = null;
            angular.forEach(repos, function(r) {
                if (r.id = repoId) repo = r;
            });

            return repo;

        },


        getRepoByName: function (repos, reponame) {
            var repo = null;
            angular.forEach(repos, function(r) {
                if (r.name = reponame) repo = r;
            });

            return repo;

        },

        getRepoByIndex: function (repos, index) {
            return repos[index];
        }
    };
});





