/*jshint undef: false, unused: false, indent: 2*/
/*global angular: false */

'use strict';

angular.module('demoApp').service('BoardDataRepository', function ($http) {

    return {
        getData: function () {
            return $http.get('/GetKanabanDetails');
        },

        addNewCard: function (cardDetails) {
            var params = {name: cardDetails.title};
            var config = {params: params}
            return $http.get('/AddNewTask', config);
        },
        issueMoved: function (issueMovedInfo) {
            var params = {issueId: issueMovedInfo.issueId,
                fromColumn: issueMovedInfo.fromColumn,
                fromPosition: issueMovedInfo.fromPosition,
                toColumn: issueMovedInfo.toColumn,
                toPosition: issueMovedInfo.toPosition}
            var config = {params: params}
            return $http.post('/IssueMoved', config);
        },
        issuePositionChanged: function (positionChangedData) {
            var params = {
                fromPosition: positionChangedData.fromPosition,
                toPostiion: positionChangedData.toPostiion,
                column: positionChangedData.column
            }
        }
        // Tu powinno być przesyłanie danych metody kontrolera, ale jak wszystko robimy we front endzie to nie wiem co z tym zrobic
    };
});


