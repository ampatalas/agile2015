/*jshint undef: false, unused: false, indent: 2*/
/*global angular: false */

'use strict';

function Board(name, numberOfColumns) {
  return {
    name: name,
    numberOfColumns: numberOfColumns,
    columns: [],
    backlogs: []
  };
}

function Column(name) {
  return {
    name: name,
    cards: []
  };
}

function Card(issue) {
  this.number = issue.number;
  this.title = issue.title;
  this.status = issue.status;
  this.details = issue.body;
  this.id = issue.id;
  this.assigneeLogin = issue.assigneeLogin;
  this.assigneeAvatar = issue.assigneeAvatar;
  this.labels = issue.labels;
  return this;
}

function Repo(githubRepo) {
  return {
    id: githubRepo.id,
    ownerLogin: githubRepo.owner.login,
    name: githubRepo.name,
    issues: [] // tabica z obiektami Issues
  }
}

function Issue(githubIssue) {
  var githubAssignee = null;
  if(githubIssue.assignee)
    githubAssignee = githubIssue.assignee.login;

  var assigneeAvatar = null;
  if(githubIssue.assignee)
    assigneeAvatar = githubIssue.assignee.avatar_url;

  var newLabels = [];
  angular.forEach(githubIssue.labels, function(githubLabel){
    newLabels.push(new Label(githubLabel));
  });

  var state = "";
  if (githubIssue.state === "closed") {
    var state = "DONE";
  } else {
    var isInProgress = false;
    angular.forEach(githubIssue.labels, function(label) {
      if(!isInProgress) {
        if (label.name == "in progress") {
          isInProgress = true;
        }
      }
    });
    if (isInProgress) {
      var state = "IN PROGRESS";
    } else {
      var state = "TO DO";
    }
  }

  return {
    number: githubIssue.number,
    id: githubIssue.id,
    title: githubIssue.title,
    body: githubIssue.body,
    state: state,
    assigneeLogin: githubAssignee,
    assigneeAvatar: assigneeAvatar,
    labels: newLabels
  }
}

function Label(githubLabel) {
  return {
    name: githubLabel.name,
    color: "#" + githubLabel.color
  }

}
