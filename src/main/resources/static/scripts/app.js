/*jshint undef: false, unused: false, indent: 2*/
/*global angular: false */


'use strict';

// Declare app level module which depends on other modules
var kanbanApp = angular.module('demoApp', [
    'as.sortable',
    'ui.bootstrap'
  ]) ;

kanbanApp.config(function($httpProvider) {
    $httpProvider.defaults.xsrfCookieName = '_csrf';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRF-Token';
});


