/*jshint undef: false, unused: false, indent: 2*/
/*global angular: false */

'use strict';

angular.module('demoApp').controller('KanbanController', ['$scope', 'GitHubDataRepository', 'GitHubDataManipulator', 'BoardService', 'BoardDataRepository', function ($scope, GitHubDataRepository, GitHubDataManipulator, BoardService, BoardDataRepository) {

    $scope.githubRepos = [];
    $scope.currentGithubRepo = null;
    $scope.userRepos = [];
    $scope.currentRepo = null;
    $scope.kanbanBoard = null;
    $scope.auth = null;
    $scope.username = "";
    $scope.userAvatar = null;
    $scope.userData = null;


    $scope.setUserRepos = function (username) {
        $scope.userRepos = [];
        GitHubDataRepository.getUserGithubRepos(username)
            .then(function (githubRepos) {
                $scope.githubRepos = githubRepos;
                angular.forEach(githubRepos, function(repo) {
                    $scope.userRepos.push(Repo(repo));
                });
                $scope.currentGithubRepo = GitHubDataManipulator.getRepoByIndex($scope.githubRepos, 0);
                $scope.currentRepo = GitHubDataManipulator.getRepoByIndex($scope.userRepos, 0);
                GitHubDataRepository.getGithubRepoIssues($scope.currentGithubRepo)
                    .then(function (issues) {
                        var newIssues = [];
                        angular.forEach(issues, function(githubIssue) {
                            newIssues.push(new Issue(githubIssue))
                        });
                        $scope.currentRepo.issues = newIssues;
                        $scope.kanbanBoard = BoardService.kanbanBoard($scope.currentRepo);
                    });
            })
    }

    // Event po wybraniu repozytorium z listy - wyswietlenie nowej tablicy
    $scope.repoSelected = function(index) {
        if ($scope.userRepos[index].id == $scope.currentRepo.id) return;
        $scope.currentRepo = GitHubDataManipulator.getRepoByIndex($scope.userRepos, index);
        $scope.currentGithubRepo = GitHubDataManipulator.getRepoByIndex($scope.githubRepos, index);
        if (!$scope.currentRepo.labels)
            GitHubDataRepository.getGithubRepoIssues($scope.currentGithubRepo)
                .then(function (issues) {
                    var newIssues = [];
                    angular.forEach(issues, function(githubIssue) {
                        newIssues.push(new Issue(githubIssue))
                    });
                    $scope.currentRepo.issues = newIssues;
                    $scope.kanbanBoard = BoardService.kanbanBoard($scope.currentRepo);
                });
        else {
            $scope.kanbanBoard = BoardService.kanbanBoard($scope.currentRepo);
        }
    }


    $scope.initBoard = function() {
        GitHubDataRepository.getUserData().then(function (auth) {
            $scope.auth = auth;
            $scope.username = auth.userId;

            GitHubDataRepository.getUserAvatar($scope.username).then(function (avatar) {
                $scope.userAvatar = avatar;
            });

            $scope.userRepos = [];
            GitHubDataRepository.getUserGithubRepos($scope.username)
                .then(function (githubRepos) {
                    $scope.githubRepos = githubRepos;
                    angular.forEach(githubRepos, function(repo) {
                        $scope.userRepos.push(Repo(repo));
                    });
                    $scope.currentGithubRepo = GitHubDataManipulator.getRepoByIndex($scope.githubRepos, 0);
                    $scope.currentRepo = GitHubDataManipulator.getRepoByIndex($scope.userRepos, 0);
                    GitHubDataRepository.getGithubRepoIssues($scope.currentGithubRepo)
                        .then(function (issues) {
                            var newIssues = [];
                            angular.forEach(issues, function(githubIssue) {
                                newIssues.push(new Issue(githubIssue))
                            });
                            $scope.currentRepo.issues = newIssues;
                            $scope.kanbanBoard = BoardService.kanbanBoard($scope.currentRepo);
                        });
                })
        });

    }

    //$scope.refreshKanban = function () {
    //    BoardService.kanbanBoard(BoardDataRepository.getData().then(function successCallback(response) {
    //        $scope.kanbanBoard = response.data;
    //    }, function errorCallback(response) {
    //        return;
    //    }));
    //}
    //
    //$scope.refreshKanban();

    $scope.kanbanSortOptions = {

        //restrict move across columns. move only within column.
        /*accept: function (sourceItemHandleScope, destSortableScope) {
         return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id;
         },*/
        itemMoved: function (event) {
            event.source.itemScope.modelValue.status = event.dest.sortableScope.$parent.column.name;
            BoardDataRepository.issueMoved({
                issueId: 25,
                fromColumn: event.source.itemScope.$parent.$parent.$parent.column.name,
                fromPosition: event.source.index,
                toColumn: event.dest.sortableScope.$parent.column.name,
                toPosition: event.dest.index
            }).then(function successCallback(response) {

            });
        },
        orderChanged: function (event) {
            BoardDataRepository.issuePositionChanged({
                fromPosition: event.source.index,
                toPostiion: event.dest.index,
                column: event.dest.sortableScope.$parent.column.name
            })
        },
        containment: '#board'
    };

    $scope.removeCard = function (column, card) {
        BoardService.removeCard($scope.kanbanBoard, column, card);
    }

    $scope.addNewCard = function (column) {
        BoardService.addNewCard($scope.kanbanBoard, column, $scope.refreshKanban);
    }

    $scope.initBoard();

}]);
