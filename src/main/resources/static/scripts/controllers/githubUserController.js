/**
 * Created by ewelina on 2016-01-16.
 */

'use strict';
kanbanApp.controller('GithubUserCtrl', ['$scope', '$http', function ($scope, $http) {

    $http.get("http://localhost:8080/user").success(function (data) {
        $scope.auth = data;
        $scope.username = data.displayName;

        getUserDetails();
        getUserRepositories();
    }).error(function () {
        $scope.error = "errro"
    })

      function getUserDetails() {
        $http.get("https://api.github.com/users/" + $scope.username)
            .success(function (data) {
                if (data.name == "") data.name = data.login;
                $scope.userData = data;
                $scope.loaded = true;
            })
            .error(function () {
                $scope.userNotFound = true;
            });
    }

    function getUserRepositories() {
         $http.get("https://api.github.com/users/" + $scope.username + "/repos").success(function (data) {
             $scope.repos = data;
             $scope.reposFound = data.length > 0;
         });
     }




}]);