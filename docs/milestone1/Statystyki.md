## 1. Miesiąc: ##
* czas oczekiwania zadania na realizację (czas wejścia do kolumny *DOING* z kolumny *TODO*)

* czas wykonywania zadania (czas wejścia do kolumny *DONE* z kolumny *DOING*)

* flow chart (jak zmieniał się status issues)
![flowchart.png](https://bitbucket.org/repo/GGEKzn/images/766248955-flowchart.png)

* średnia ilość tasków w kolumnie *DOING* (sprawdzanie, ile tasków było każdego dnia w ciągu ostatniego miesiąca)

## 2. Milestone: ##
* taski do zrobienia, wykonane, realizowane.
* liczba tasków re-opened
* liczba tasków niezrealizowanych w terminie


