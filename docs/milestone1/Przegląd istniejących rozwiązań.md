# Kanbanowa tablica bazująca na issues z [GitHuba](https://github.com/) #

Podczas przeglądu istniejących rozwiązań natrafiliśmy na kilka narzędzi, które umożliwiają zorganizować issues z platformy GitHub, tak aby proces zarządzania i wytwarzania oprogramowania miał bardziej zwinny charakter. Poniżej krótko opisaliśmy według nas najciekawsze rozwiązania.


## [Waffle.io](https://waffle.io/) ##
![waffle.png](https://bitbucket.org/repo/GGEKzn/images/1432936462-waffle.png)
[źródło](http://blog.particle.io/2014/02/24/sprint-six/)

Platforma ta jest bardzo zautomatyzowana – już po zarejestrowaniu się automatycznie tworzy tablicę bazującą na issue z Githuba. Widok domyślny oferuje przypisanie issue do jednej z 4 kolumn: backlog, ready, in progres, done. Istnieje także możliwość zmiany kolumn, tak aby dostosować tablicę do własnego procesu. Innymi funkcjami są proste dodawanie nowych issue i możliwość zmiany ich statusu poprzez przeciąganie i upuszczanie na danej kolumnie.

Waffle oferuje także mierzenie wydajności - pokazuje liczbę issue wykonanej w określonym czasie (podział na tygodnie). Metryka ta jest przedstawiona w formie prostego wykresu, który umożliwia filtrowanie po etykietach (np. bug, enhancement, invalid) i milestonach. Opcja mierzenia wydajności jest przydatną opcją gdy chcemy określić rytm pracy zespołu i pomaga przewidzieć ile zadań zespół jest w stanie wykonać.

Minusem jaki udało nam się zauważyć jest brak możliwości podglądu takiej tablicy dla konkretnego użytkownika – możliwy jest tylko podgląd dla całego projektu. Waffle pozwala na ocenę pracy całego zespołu, ale nie pozwala na uzyskanie informacji o zadaniach konkretnego pracownika.

***Podsumowanie:***

*Niezaprzeczalnie główną zaletą tej aplikacji jest przyjazny interfejs użytkownika, oferujący łatwe w użyciu funkcje i automatyzację procesu tworzenia tablicy. Waffle pozwala na monitorowanie pracy całego zespołu.*


## [Huboard.com](https://huboard.com/) ##
![huboard.gif](https://bitbucket.org/repo/GGEKzn/images/3187875241-huboard.gif)
[źródło](https://gist.github.com/rauhryan/5f3eeb75fe1237a02a26)

Platforma pod wieloma względami podobna do Waffle, jednak z nieco innymi funkcjonalnościami. Główne różnice polegają na innym interfejsie – nadal jest to tablica kanbanowa z podziałem na kolumny, które można dostosować do własnych potrzeb, ale wygląd interfejsu jest mniej przyjazny dla oka.

Huboard jednak okazuje się lepszy jeśli chodzi o filtrację issues na kanbanowej tablicy – możemy filtrować np. po etykietach czy issue przydzielonych do użytkownika.

Dodawanie nowych issue też jest zrobione lepiej, ponieważ ma więcej opcji niż ta sama funkcjonalność w Waffle. Możemy nie tylko dodać nowy issue, ale także od razu przypisać mu odpowiednie etykiety i przydzielić do konkretnej osoby.

Podobnie jak i Waffle, Huboard udostępnia takie funkcje jak przeciągnij i upuść, dzięki czemu można łatwo zmieniać położenie w kolumnie i statusy issues.

***Podsumowanie:***

*Nieco więcej funkcjonalności kosztem mniej ładnego interfejsu. Brak statystyk.*


Podobne rozwiązania:

* [sprint.ly](https://sprint.ly/)

* [zenhub.io](https://www.zenhub.io/)

* [taiga.io](https://taiga.io/)
