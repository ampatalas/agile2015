# AgileHub [![Build Status](https://drone.io/bitbucket.org/ampatalas/agile2015/status.png)](https://drone.io/bitbucket.org/ampatalas/agile2015/latest)#
Projekt powstaje w ramach realizacji kursu ze *Zwinnych metodyk wytwarzania oprogramowania*.
Celem projektu jest stworzenie platformy, która ułatwi wdrążenie zwinnych metodyk wytwarzania oprogramowania do dowolnego projektu tworzonego na platformie GitHub.

Nasza aplikacja będzie odpowiednikiem kanbanowej tablicy – jednego z narzędzi wykorzystywanego w metodzie kanban do zarządzania procesami i projektami w organizacjach. Tablica ta będzie opierać się o jedną z najpopularniejszych platform przeznaczonych dla projektów programistycznych – GitHub.



## Sekcje ##
[Przegląd istniejących rozwiązań](Przegląd istniejących rozwiązań)

[Nasz projekt](Szczegóły projektu)

[Wybrane metryki do implementacji](Statystyki)

[Metodyka](Metodyka)

[Iteracje](Plan iteracji)