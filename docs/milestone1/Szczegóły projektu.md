Po researchu istniejących rozwiązań wyłoniliśmy kilka funkcji, które są niezbędne do stworzenia w pełni funkcjonalnej tablicy kanbanowej opartej o platformę GitHub.

Główne funkcjonalności jakie planujemy zrealizować w naszej aplikcji:

1. **Tablica kanbanowa** - issues wyświetlane dla zalogowanego użytkownika w trzech kolumnach: *TODO*, *DOING*, *DONE* z podziałem na milestone.

2. **Zarzadzanie issue** – dodanie nowego issue, nadawanie etykiet, zmiana statusu poprzez przeciąganie między kolumnami

3. **Synchronizacja z platformą GitHub** - logowanie za pomocą konta użytkownika GitHub; pobieranie issue z Githuba, synchronizacja zmian w issue (np. edycja, aktualizacja statusu)

4. [**Panel statystyk**](Statystyki)


# Wykorzystywane technologie: #
* Java EE
* Spring Framework
* Hibernate
* MySQL
* Maven
* Tomcat
* JavaScript

