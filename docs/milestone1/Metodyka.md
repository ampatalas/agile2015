# Opis metodyki #

Oryginalna metodyka na potrzeby projektu, zapożyczająca elementy ze SCRUM i XP, jednak o niskim stopniu formalizacji.

# Iteracje #
Długość iteracji to **2 tygodnie**.

1. Iteracja zaczyna się zaplanowaniem zadań na dana iteracje (w trakcie zajęć).

2. Co tydzień odbywa się krótkie podsumowanie tego, co się zrobiło w danym tygodniu, i co się zrobi w przyszłym (zapożyczone ze SCRUM).

3. Iteracja kończy się retrospekcja (w trakcie zajęć):

- Czego nowego się nauczyliśmy?
- Co się nam nie podobało i co chcemy poprawić?


Pełna rozpiska zaplanowanych zadań na poszczególne iteracje znajduje się w [osobnym dokumencie](https://bitbucket.org/agile15_2/agile15_2/wiki/Plan%20iteracji).

# Review #
Posiadamy trzy branche:

- master - główny branch, ze sprawdzonym, stabilnym kodem, wersja CI
- development - test z kodem do sprawdzenia, wersja do code review
- learning - test na trening i próbowanie nowych rzeczy

Dla nowych funkcjonalności najlepiej jest tworzyć osobne branche, z opisem, do którego issue się odnoszą, np. 
```
#!txt

fixes #1
closes #2
finishes #3
```

# Kolaboracja #
Projekt ma naturę zdalną (poza spotkaniami na początek i koniec iteracji), ale w niektórych przypadkach może być potrzebne wspólne rozwiązywanie problemów, np. rozpoznanie nowego frameworka.

W takich sytuacjach należy uzgnodnić termin na pair programming.

# Continuous Integration [![Build Status](https://drone.io/bitbucket.org/ampatalas/agile2015/status.png)](https://drone.io/bitbucket.org/ampatalas/agile2015/latest) #
Z użyciem drone.io -  wybranym ze względu na darmowość, łatwą interację z bitbucket oraz wsparcie dla Javy 8 i Mavena (technologii używanych w projekcie).

[Publiczne repozytorium dla drone.io](https://bitbucket.org/ampatalas/agile2015)

[Projekt na drone.io](https://drone.io/bitbucket.org/ampatalas/agile2015)

# Testy jednostkowe #
Część pierwsza projektu, tablica Kanban, ma format graficzny. Statystki jednak będą tworzone z użyciem testów jednostkowych:

- JUnit
- Mockito (mockowanie warstwy danych)