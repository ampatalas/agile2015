# 1: 26.10 - 9.11 #
1. Zaplanowanie statystyk ![chk.gif](https://bitbucket.org/repo/GGEKzn/images/3313251862-chk.gif)
2. Github API + logowanie + MVC
3. Set up bazy danych ![chk.gif](https://bitbucket.org/repo/GGEKzn/images/3313251862-chk.gif)
4. Rozeznanie w Javascript/CSS

# 2: 9.11 - 23.11 #
1. Tworzenie tablicy kanbanowej
2. Obliczenia statystyk - back-end

# 3: 23.11 - 7.12 #
1. Tablica kanbanowa - kończenie
2. Logika statystyk

# 4: 7.12 - 21.12 #
1. Statystyki - UI